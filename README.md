itel-fw-extractor
=================

A tool for Itel (currently A16 Plus) firmware extraction (.pac file).


Features
--------

 * List firmware content 
 * Extract all files
 * Extract specific files
 * Pretty display the main header
 * Pretty display any file sub-header


Installation
------------

```$ cargo install --git https://framagit.org/rogarb/itel-fw-extractor itel-fw-extractor```


Usage
-----

See ```itel-fw-extractor --help```


Credits
-------

This program is based on code from [https://github.com/echo-devim/huextract]
(contributed by myself).
The header structures have been adapted from the code freely available at:
[https://spdflashtool.com/source/spd-tool-source-code] (especially the file
```binpack.h```).

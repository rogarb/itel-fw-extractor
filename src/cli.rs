//! This module defines the command line interface of the program using the clap
//! library.
//!
use clap::{Args, Parser, Subcommand};

use std::path::PathBuf;

use crate::error::Error;
use crate::input::Input;
use CliCommand::*;

/// Extract the information contained in a .pac file used to store Itel A16 Plus
/// firmwares.
#[derive(Parser)]
pub struct Cli {
    /// The command to execute.
    #[clap(subcommand)]
    command: CliCommand,
    /// The name of the file to extract.
    #[clap(value_parser)]
    input: PathBuf,
}

/// The command to execute.
///
/// Defaults to extract-imgs.
#[derive(Subcommand)]
enum CliCommand {
    /// Extract the files contained in the input file.
    Extract(Options),
    /// List the files contained in the input file.
    List,
    /// Displays the raw content of the main header, hex formatted
    PrintRawHeader,
    /// Displays the raw content of a file header given its position in the file, hex formatted
    PrintFileHeader { index: usize },
}

#[derive(Args)]
pub struct Options {
    /// Specify file by index (see the output of the list command).
    ///
    /// This option can be specified multiple times to process multiple files.
    #[clap(short, long, conflicts_with = "exclude", multiple = true)]
    pub index: Vec<usize>,
    /// Exclude file by index (see the output of the list command).
    ///
    /// This option can be specified multiple times to process multiple files.
    #[clap(short = 'x', long, conflicts_with = "index", multiple = true)]
    pub exclude: Vec<usize>,
    /// Overwrite existing files.
    #[clap(short, long)]
    pub force: bool,
}

impl Cli {
    pub fn run() -> Result<(), Error> {
        let cli = Self::parse();

        let mut input = Input::try_from(cli.input.as_path())?;
        match cli.command {
            Extract(options) => input.extract_files(options)?,
            List => {
                println!("{}", input)
            }
            PrintRawHeader => {
                println!("{}", input.raw_header()?)
            }
            PrintFileHeader { index } => {
                println!("{}", input.raw_file_header(index)?)
            }
        }

        Ok(())
    }
}

//! Module defining the Error type used in the program
//!
use std::array::TryFromSliceError;
use std::fmt::Display;
use std::string::FromUtf16Error;

#[derive(Debug, PartialEq)]
pub struct Error {
    msg: String,
}

impl Error {
    pub fn new(msg: String) -> Self {
        Self { msg }
    }
}

impl Display for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "Error: {}", self.msg)
    }
}

impl From<std::io::Error> for Error {
    fn from(error: std::io::Error) -> Self {
        Self {
            msg: format!("IO error (kind: {}): {error}", error.kind()),
        }
    }
}

impl From<std::string::FromUtf8Error> for Error {
    fn from(e: std::string::FromUtf8Error) -> Self {
        Self {
            msg: format!("conversion error: {e}"),
        }
    }
}

impl From<&'static str> for Error {
    fn from(s: &'static str) -> Self {
        Self { msg: s.to_owned() }
    }
}

impl From<FromUtf16Error> for Error {
    fn from(e: FromUtf16Error) -> Self {
        Self {
            msg: format!("UTF-16 conversion error: {e}"),
        }
    }
}

impl From<TryFromSliceError> for Error {
    fn from(e: TryFromSliceError) -> Self {
        Self {
            msg: format!("slice conversion error: {e}"),
        }
    }
}

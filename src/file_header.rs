//! This module describes the header of a file packed in a .pac file.
//!
//! Header structure:
//! - header size: 4 bytes
//! - file ID: 512 bytes (contains a string in a [u16 ; 256] array)
//! - file name: 512 bytes (contains a string in a [u16 ; 256] array)
//! - file version: 512 bytes (contains a string in a [u16 ; 256] array, commented to reserved)
//! - file size: 4 bytes
//! - file flag: 4 bytes (set to 1: the object is a file, set to 0: it is an operation coded in
//!   file ID)
//! - check flag: 4 bytes (set to 1, the file must be downloaded; set to 0, the file can't be
//!   downloaded)
//! - data offset: 4 bytes (offset from the packet header to the begining of the data)
//! - omit flag: 4 bytes (if set to 1, the file is ignored by the SPD tool)
//! - address number: 4 bytes
//! - address: 20 bytes (as [u32 ; 5] array)
//! - reserved area: 1036 bytes (as [u32 ; 249] array)
//!
//! => Total size: 2620 bytes
//!
//! This is based on the information contained in the header file binpack.h
//! available from [https://spdflashtool.com/source/spd-tool-source-code]
//!

use crate::utils::{boolify, stringify_u16, vectorize_u32};

pub mod display;

/// The structure holding the parsed version of the file header.
#[derive(Clone)]
pub struct FileHeader {
    pub header_size: u32,
    pub file_id: String,
    pub file_name: String,
    pub file_version: String,
    pub file_size: u32,
    pub file_flag: bool,
    pub check_flag: bool,
    pub data_offset: u32,
    pub omit_flag: bool,
    pub address_number: u32,
    pub address: [u32; 5],
    pub reserved: [u32; 249],
}

/// The raw version of the file header
pub struct RawFileHeader {
    header_size: [u8; 4],
    file_id: [u8; 512],
    file_name: [u8; 512],
    file_version: [u8; 512],
    file_size: [u8; 4],
    file_flag: [u8; 4],
    check_flag: [u8; 4],
    data_offset: [u8; 4],
    omit_flag: [u8; 4],
    address_number: [u8; 4],
    address: [u8; 20],
    reserved: [u8; 1036],
}

pub const FILE_HEADER_LEN: usize = std::mem::size_of::<RawFileHeader>();

impl TryFrom<&[u8]> for FileHeader {
    type Error = crate::error::Error;
    fn try_from(data: &[u8]) -> Result<Self, Self::Error> {
        let raw = RawFileHeader::try_from(data)?;
        Ok(Self {
            header_size: u32::from_le_bytes(raw.header_size),
            file_id: stringify_u16(raw.file_id.as_slice())?,
            file_name: stringify_u16(raw.file_name.as_slice())?,
            file_version: stringify_u16(raw.file_version.as_slice())?,
            file_size: u32::from_le_bytes(raw.file_size),
            file_flag: boolify(raw.file_flag.as_slice())?,
            check_flag: boolify(raw.check_flag.as_slice())?,
            data_offset: u32::from_le_bytes(raw.data_offset),
            omit_flag: boolify(raw.omit_flag.as_slice())?,
            address_number: u32::from_le_bytes(raw.address_number),
            address: vectorize_u32(raw.address.as_slice())?
                .try_into()
                .map_err(|e| Self::Error::new(format!("{e:?}")))?,
            reserved: [0; 249],
            // reserved: vectorize_u32(raw.reserved.as_slice())?
            //     .try_into()
            //     .map_err(|e| Self::Error::new(format!("{e:?}")))?,
        })
    }
}

impl TryFrom<&[u8]> for RawFileHeader {
    type Error = crate::error::Error;
    fn try_from(data: &[u8]) -> Result<Self, Self::Error> {
        if data.len() < FILE_HEADER_LEN {
            Err(Self::Error::new(format!(
                "not enough data to parse a .pac header ({} bytes provided)",
                data.len()
            )))
        } else {
            Ok(Self {
                header_size: data[0..4].try_into()?,           // 4
                file_id: data[4..516].try_into()?,             // 512
                file_name: data[516..1028].try_into()?,        // 512
                file_version: data[1028..1540].try_into()?,    // 512
                file_size: data[1540..1544].try_into()?,       // 4
                file_flag: data[1544..1548].try_into()?,       // 4
                check_flag: data[1548..1552].try_into()?,      // 4
                data_offset: data[1552..1556].try_into()?,     // 4
                omit_flag: data[1556..1560].try_into()?,       // 4
                address_number: data[1560..1564].try_into()?,  // 4
                address: data[1564..1584].try_into()?,         // 20
                reserved: data[1584..1584 + 1036].try_into()?, // 1036
            })
        }
    }
}

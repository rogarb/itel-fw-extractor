//! This module defines the implementation of the Display trait for ImgHeader
//!
use super::{FileHeader, RawFileHeader};
use tabled::{Table, Tabled};

use crate::tabled_types::ArrayValue;

#[derive(Tabled, Default)]
pub struct TableEntry {
    #[tabled(rename = "Index")]
    index: usize,
    #[tabled(rename = "Header size (bytes)")]
    header_size: u32,
    #[tabled(rename = "File ID")]
    file_id: String,
    #[tabled(rename = "File name")]
    file_name: String,
    #[tabled(rename = "File version")]
    file_version: String,
    #[tabled(rename = "File size (bytes)")]
    file_size: u32,
    #[tabled(rename = "File flag")]
    file_flag: bool,
    #[tabled(rename = "Check flag")]
    check_flag: bool,
    #[tabled(rename = "Data offset")]
    data_offset: u32,
    #[tabled(rename = "Omit flag")]
    omit_flag: bool,
    #[tabled(rename = "Address number")]
    address_number: u32,
    #[tabled(rename = "File size + File offset")]
    total: u32,
    // #[tabled(rename = "Address")]
    // address: [u32; 5],
    // #[tabled(rename = "Reserved")]
    // reserved: [u32; 249],
}

impl From<&FileHeader> for TableEntry {
    fn from(header: &FileHeader) -> Self {
        TableEntry {
            header_size: header.header_size,
            file_id: header.file_id.to_owned(),
            file_name: header.file_name.to_owned(),
            file_version: header.file_version.to_owned(),
            file_size: header.file_size,
            file_flag: header.file_flag,
            check_flag: header.check_flag,
            data_offset: header.data_offset,
            omit_flag: header.omit_flag,
            address_number: header.address_number,
            total: header.file_size + header.data_offset,
            // address: header.address,
            // reserved: header.reserved,
            ..Self::default()
        }
    }
}

impl TableEntry {
    pub fn with_index(header: &FileHeader, index: usize) -> Self {
        let mut new = Self::from(header);
        new.index = index;
        new
    }
}

impl std::fmt::Display for FileHeader {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let entries = TableEntry::from(self);
        let table = Table::new(vec![entries]);

        write!(f, "{}", table)
    }
}

#[derive(Tabled)]
pub struct RawTableEntry {
    #[tabled(rename = "Header size (bytes)")]
    header_size: ArrayValue<2>,
    #[tabled(rename = "File ID")]
    file_id: ArrayValue<2>,
    #[tabled(rename = "File name")]
    file_name: ArrayValue<2>,
    #[tabled(rename = "File version")]
    file_version: ArrayValue<2>,
    #[tabled(rename = "File size (bytes)")]
    file_size: ArrayValue<4>,
    #[tabled(rename = "File flag")]
    file_flag: ArrayValue<4>,
    #[tabled(rename = "Check flag")]
    check_flag: ArrayValue<4>,
    #[tabled(rename = "Data offset")]
    data_offset: ArrayValue<4>,
    #[tabled(rename = "Omit flag")]
    omit_flag: ArrayValue<4>,
    #[tabled(rename = "Address number")]
    address_number: ArrayValue<1>,
    #[tabled(rename = "Address")]
    address: ArrayValue<1>,
    #[tabled(rename = "Reserved")]
    reserved: ArrayValue<4>,
}

impl From<&RawFileHeader> for RawTableEntry {
    fn from(header: &RawFileHeader) -> Self {
        Self {
            header_size: header.header_size.into(),
            file_id: header.file_id.into(),
            file_name: header.file_name.into(),
            file_version: header.file_version.into(),
            file_size: header.file_size.into(),
            file_flag: header.file_flag.into(),
            check_flag: header.check_flag.into(),
            data_offset: header.data_offset.into(),
            omit_flag: header.omit_flag.into(),
            address_number: header.address_number.into(),
            address: header.address.into(),
            reserved: header.reserved.into(),
        }
    }
}

impl std::fmt::Display for RawFileHeader {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let entries = RawTableEntry::from(self);
        let table = Table::new(vec![entries]);

        write!(f, "{}", table)
    }
}

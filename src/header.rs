//! This module describes the header of a .pac file, used to store Itel A16 plus
//! firmware
//!
//! Header structure:
//! - packet structure version: 48 bytes (contains a string in a [u16 ; 24] array)
//! - whole packet size: 4 bytes
//! - product name: 512 bytes (contains a string in a [u16 ; 256] array)
//! - product version: 512 bytes (contains a string in a [u16 ; 256] array)
//! - number of files in the package: 4 bytes
//! - offset to the table of file headers: 4 bytes
//! - mode: 4 bytes
//! - flash type: 4 bytes
//! - nand strategy: 4 bytes
//! - nv backup flag: 4 bytes (probably a boolean)
//! - nand page type: 4 bytes
//! - product alias: 200 bytes (contains a string in a [u16 ; 100] array)
//! - OMA Device Management product flag: 4 bytes (see
//!   [https://en.wikipedia.org/wiki/OMA_Device_Management])
//! - OMA managed flag: 4 bytes (probably a boolean)
//! - preload flag: 4 bytes (probably a boolean)
//! - reserved area: 800 bytes (a [u32 ; 200] array)
//! - magic field: 4 bytes (0xFF 0xFA 0xFF 0xFA)
//! - CRC field 1: 2 bytes
//! - CRC field 2: 2 bytes
//!
//! Total size => 2124 bytes
//!
//! This is based on the information contained in the header file binpack.h
//! available from [https://spdflashtool.com/source/spd-tool-source-code]
//!

use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;

use crate::utils::{boolify, stringify_u16, vectorize_u32};

mod display;

/// The structure holding the raw package header.
pub struct RawPacHeader {
    version: [u8; 48],
    size: [u8; 4],
    product_name: [u8; 512],
    product_version: [u8; 512],
    file_count: [u8; 4],
    filetable_offset: [u8; 4],
    mode: [u8; 4],
    flash_type: [u8; 4],
    nand_strategy: [u8; 4],
    nv_backup_flag: [u8; 4],
    nand_page_type: [u8; 4],
    product_alias: [u8; 200],
    oma_dm_flag: [u8; 4],
    oma_managed_flag: [u8; 4],
    preload_flag: [u8; 4],
    reserved: [u8; 800],
    magic_field: [u8; 4],
    crc1: [u8; 2],
    crc2: [u8; 2],
}

// This is needed as Default can't be derived due to big arrays
impl Default for RawPacHeader {
    fn default() -> Self {
        Self {
            version: [0; 48],
            size: [0; 4],
            product_name: [0; 512],
            product_version: [0; 512],
            file_count: [0; 4],
            filetable_offset: [0; 4],
            mode: [0; 4],
            flash_type: [0; 4],
            nand_strategy: [0; 4],
            nv_backup_flag: [0; 4],
            nand_page_type: [0; 4],
            product_alias: [0; 200],
            oma_dm_flag: [0; 4],
            oma_managed_flag: [0; 4],
            preload_flag: [0; 4],
            reserved: [0; 800],
            magic_field: [0; 4],
            crc1: [0; 2],
            crc2: [0; 2],
        }
    }
}

/// The structure holding the parsed package header.
pub struct PacHeader {
    pub version: String,
    pub size: u32,
    pub product_name: String,
    pub product_version: String,
    pub file_count: u32,
    pub filetable_offset: u32,
    pub mode: u32,
    pub flash_type: u32,
    pub nand_strategy: u32,
    pub nv_backup_flag: bool,
    pub nand_page_type: u32,
    pub product_alias: String,
    pub oma_dm_flag: u32,
    pub oma_managed_flag: bool,
    pub preload_flag: bool,
    pub reserved: [u32; 200],
    pub magic_field: u32,
    pub crc1: u16,
    pub crc2: u16,
}

pub const PAC_HEADER_LEN: usize = std::mem::size_of::<RawPacHeader>();

impl TryFrom<&mut BufReader<File>> for PacHeader {
    type Error = crate::error::Error;
    fn try_from(file: &mut BufReader<File>) -> Result<Self, Self::Error> {
        let mut buf = vec![0; PAC_HEADER_LEN];
        file.read_exact(&mut buf)?;
        let raw = RawPacHeader::try_from(buf.as_slice())?;
        Ok(Self {
            version: stringify_u16(raw.version.as_slice())?,
            size: u32::from_le_bytes(raw.size),
            product_name: stringify_u16(raw.product_name.as_slice())?,
            product_version: stringify_u16(raw.product_version.as_slice())?,
            file_count: u32::from_le_bytes(raw.file_count),
            filetable_offset: u32::from_le_bytes(raw.filetable_offset),
            mode: u32::from_le_bytes(raw.mode),
            flash_type: u32::from_le_bytes(raw.flash_type),
            nand_strategy: u32::from_le_bytes(raw.nand_strategy),
            nv_backup_flag: boolify(raw.nv_backup_flag.as_slice())?,
            nand_page_type: u32::from_le_bytes(raw.nand_page_type),
            product_alias: stringify_u16(raw.product_alias.as_slice())?,
            oma_dm_flag: u32::from_le_bytes(raw.oma_dm_flag),
            oma_managed_flag: boolify(raw.oma_managed_flag.as_slice())?,
            preload_flag: boolify(raw.preload_flag.as_slice())?,
            reserved: vectorize_u32(raw.reserved.as_slice())?
                .try_into()
                .map_err(|e| Self::Error::new(format!("{e:?}")))?,
            magic_field: u32::from_le_bytes(raw.magic_field),
            crc1: u16::from_le_bytes(raw.crc1),
            crc2: u16::from_le_bytes(raw.crc2),
        })
    }
}

impl TryFrom<&[u8]> for RawPacHeader {
    type Error = crate::error::Error;
    fn try_from(data: &[u8]) -> Result<Self, Self::Error> {
        if data.len() < PAC_HEADER_LEN {
            Err(Self::Error::new(format!(
                "not enough data to parse a .pac header ({} bytes provided)",
                data.len()
            )))
        } else {
            Ok(Self {
                version: data[0..48].try_into()?,               // 48 bytes
                size: data[48..52].try_into()?,                 // 4 bytes
                product_name: data[52..564].try_into()?,        // 512 bytes
                product_version: data[564..1076].try_into()?,   // 512 bytes
                file_count: data[1076..1080].try_into()?,       // 4 bytes
                filetable_offset: data[1080..1084].try_into()?, // 4 bytes
                mode: data[1084..1088].try_into()?,             // 4 bytes
                flash_type: data[1088..1092].try_into()?,       // 4 bytes
                nand_strategy: data[1092..1096].try_into()?,    // 4 bytes
                nv_backup_flag: data[1096..1100].try_into()?,   // 4 bytes
                nand_page_type: data[1100..1104].try_into()?,   // 4 bytes
                product_alias: data[1104..1304].try_into()?,    // 200 bytes
                oma_dm_flag: data[1304..1308].try_into()?,      // 4 bytes
                oma_managed_flag: data[1308..1312].try_into()?, // 4 bytes
                preload_flag: data[1312..1316].try_into()?,     // 4 bytes
                reserved: data[1316..2116].try_into()?,         // 800 bytes
                magic_field: data[2116..2120].try_into()?,      // 4 bytes
                crc1: data[2120..2122].try_into()?,             // 2 bytes
                crc2: data[2122..2124].try_into()?,             // 2 bytes
            })
        }
    }
}

//! This module defines the implementation of the Display trait for PacHeader
//!
use super::{PacHeader, RawPacHeader};
use tabled::{object::Columns, Modify, Style, Table, Tabled, Width};

use crate::tabled_types::ArrayValue;

#[derive(Tabled)]
pub struct TableEntry {
    #[tabled(rename = "Version string")]
    version: String,
    #[tabled(rename = "Total size (bytes)")]
    size: u32,
    #[tabled(rename = "Product name")]
    product_name: String,
    #[tabled(rename = "Product version")]
    product_version: String,
    #[tabled(rename = "File count")]
    file_count: u32,
    #[tabled(rename = "File data offset (bytes)")]
    filetable_offset: u32,
    #[tabled(rename = "Mode")]
    mode: u32,
    #[tabled(rename = "Flash type")]
    flash_type: u32,
    #[tabled(rename = "Nand strategy")]
    nand_strategy: u32,
    #[tabled(rename = "NV backup flag")]
    nv_backup_flag: bool,
    #[tabled(rename = "Nand page type")]
    nand_page_type: u32,
    #[tabled(rename = "Product alias")]
    product_alias: String,
    #[tabled(rename = "OMA management flag")]
    oma_dm_flag: u32,
    #[tabled(rename = "OMA managed")]
    oma_managed_flag: bool,
    #[tabled(rename = "Preload flag")]
    preload_flag: bool,
    //reserved: [u32; 200],
    #[tabled(rename = "Magic bytes")]
    magic_field: u32,
    #[tabled(rename = "CRC 1")]
    crc1: u16,
    #[tabled(rename = "CRC 2")]
    crc2: u16,
}

impl std::fmt::Display for PacHeader {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let entries = TableEntry::from(self);
        let table = Table::new(vec![entries])
            .with(Style::psql())
            .with(
                Modify::new(Columns::single(1)).with(Width::wrap("Total size".len()).keep_words()),
            )
            .with(Modify::new(Columns::single(4)).with(Width::wrap("count".len()).keep_words()))
            .with(Modify::new(Columns::single(5)).with(Width::wrap("File data".len()).keep_words()))
            .with(Modify::new(Columns::single(7)).with(Width::wrap("Flash".len()).keep_words()))
            .with(Modify::new(Columns::single(8)).with(Width::wrap("strategy".len()).keep_words()))
            .with(Modify::new(Columns::single(9)).with(Width::wrap("NV backup".len()).keep_words()))
            .with(Modify::new(Columns::single(10)).with(Width::wrap("Nand".len()).keep_words()))
            .with(
                Modify::new(Columns::single(12)).with(Width::wrap("management".len()).keep_words()),
            )
            .with(Modify::new(Columns::single(13)).with(Width::wrap("managed".len()).keep_words()))
            .with(Modify::new(Columns::single(14)).with(Width::wrap("Preload".len()).keep_words()));

        write!(f, "{}", table)
    }
}

impl From<&PacHeader> for TableEntry {
    fn from(header: &PacHeader) -> Self {
        TableEntry {
            version: header.version.to_owned(),
            size: header.size,
            product_name: header.product_name.to_owned(),
            product_version: header.product_version.to_owned(),
            file_count: header.file_count,
            filetable_offset: header.filetable_offset,
            mode: header.mode,
            flash_type: header.flash_type,
            nand_strategy: header.nand_strategy,
            nv_backup_flag: header.nv_backup_flag,
            nand_page_type: header.nand_page_type,
            product_alias: header.product_alias.to_owned(),
            oma_dm_flag: header.oma_dm_flag,
            oma_managed_flag: header.oma_managed_flag,
            preload_flag: header.preload_flag,
            //reserved: header.reserved,
            magic_field: header.magic_field,
            crc1: header.crc1,
            crc2: header.crc2,
        }
    }
}

/// Table entry related to RawPacHeader
#[derive(Tabled)]
pub struct RawTableEntry {
    #[tabled(rename = "Version string")]
    version: ArrayValue<2>,
    #[tabled(rename = "Total size (bytes)")]
    size: ArrayValue<1>,
    #[tabled(rename = "Product name")]
    product_name: ArrayValue<4>,
    #[tabled(rename = "Product version")]
    product_version: ArrayValue<4>,
    #[tabled(rename = "File count")]
    file_count: ArrayValue<4>,
    #[tabled(rename = "File data offset (bytes)")]
    filetable_offset: ArrayValue<1>,
    #[tabled(rename = "Mode")]
    mode: ArrayValue<1>,
    #[tabled(rename = "Flash type")]
    flash_type: ArrayValue<1>,
    #[tabled(rename = "Nand strategy")]
    nand_strategy: ArrayValue<1>,
    #[tabled(rename = "NV backup flag")]
    nv_backup_flag: ArrayValue<1>,
    #[tabled(rename = "Nand page type")]
    nand_page_type: ArrayValue<1>,
    #[tabled(rename = "Product alias")]
    product_alias: ArrayValue<4>,
    #[tabled(rename = "OMA management flag")]
    oma_dm_flag: ArrayValue<1>,
    #[tabled(rename = "OMA managed")]
    oma_managed_flag: ArrayValue<1>,
    #[tabled(rename = "Preload flag")]
    preload_flag: ArrayValue<1>,
    //#[tabled(rename = "Reserved Array", skip)]
    //reserved: ArrayValue<4>,
    #[tabled(rename = "Magic bytes")]
    magic_field: ArrayValue<1>,
    #[tabled(rename = "CRC 1")]
    crc1: ArrayValue<1>,
    #[tabled(rename = "CRC 2")]
    crc2: ArrayValue<1>,
}

impl From<&RawPacHeader> for RawTableEntry {
    fn from(header: &RawPacHeader) -> Self {
        RawTableEntry {
            version: header.version.into(),
            size: header.size.into(),
            product_name: header.product_name.into(),
            product_version: header.product_version.into(),
            file_count: header.file_count.into(),
            filetable_offset: header.filetable_offset.into(),
            mode: header.mode.into(),
            flash_type: header.flash_type.into(),
            nand_strategy: header.nand_strategy.into(),
            nv_backup_flag: header.nv_backup_flag.into(),
            nand_page_type: header.nand_page_type.into(),
            product_alias: header.product_alias.to_owned().into(),
            oma_dm_flag: header.oma_dm_flag.into(),
            oma_managed_flag: header.oma_managed_flag.into(),
            preload_flag: header.preload_flag.into(),
            //reserved: header.reserved.into(),
            magic_field: header.magic_field.into(),
            crc1: header.crc1.into(),
            crc2: header.crc2.into(),
        }
    }
}

impl std::fmt::Display for RawPacHeader {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let entries = RawTableEntry::from(self);
        let table = Table::new(vec![entries])
            .with(Style::psql())
            .with(
                Modify::new(Columns::single(1)).with(Width::wrap("Total size".len()).keep_words()),
            )
            .with(Modify::new(Columns::single(4)).with(Width::wrap("count".len()).keep_words()))
            .with(Modify::new(Columns::single(5)).with(Width::wrap("File data".len()).keep_words()))
            .with(Modify::new(Columns::single(7)).with(Width::wrap("Flash".len()).keep_words()))
            .with(Modify::new(Columns::single(8)).with(Width::wrap("strategy".len()).keep_words()))
            .with(Modify::new(Columns::single(9)).with(Width::wrap("NV backup".len()).keep_words()))
            .with(Modify::new(Columns::single(10)).with(Width::wrap("Nand".len()).keep_words()))
            .with(
                Modify::new(Columns::single(12)).with(Width::wrap("management".len()).keep_words()),
            )
            .with(Modify::new(Columns::single(13)).with(Width::wrap("managed".len()).keep_words()))
            .with(Modify::new(Columns::single(14)).with(Width::wrap("Preload".len()).keep_words()));

        write!(f, "{}", table)
    }
}

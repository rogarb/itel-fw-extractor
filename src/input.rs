//! This module abstracts the input file.
//!
use std::fs::File;
use std::io::prelude::*;
use std::io::{BufReader, SeekFrom};
use std::path::Path;

use crate::cli::Options;
use crate::error::Error;
use crate::file_header::{FileHeader, RawFileHeader, FILE_HEADER_LEN};
use crate::header::{PacHeader, RawPacHeader, PAC_HEADER_LEN};

mod display;

pub struct Input {
    /// Buffer containing the input data
    data: BufReader<File>,
    /// Parsed header
    header: PacHeader,
    /// Vector containing the different headers and their offset
    parts: Vec<FileHeader>,
    /// Size of the input file
    pub size: u64,
    /// File name we got the data from
    filename: String,
}

impl std::convert::TryFrom<&Path> for Input {
    type Error = Error;
    /// Create an instance of Input from a Path
    fn try_from(path: &Path) -> Result<Self, Self::Error> {
        let file = File::open(path)?;
        let size = file.metadata()?.len();
        let mut data = BufReader::new(file);
        let header = PacHeader::try_from(&mut data)?;
        let mut new = Input {
            data,
            size,
            header,
            parts: Vec::new(),
            filename: format!("{}", path.display()),
        };

        new.parse()?;
        Ok(new)
    }
}

impl Input {
    /// Get the headers of the packed img files.
    ///
    /// Returns a Vec<Img>.
    pub fn parse(&mut self) -> Result<(), Error> {
        let mut offset = self.header.filetable_offset as u64;
        while self.parts.len() < self.header.file_count as usize {
            let mut buf = [0; FILE_HEADER_LEN as usize];
            self.data.seek(SeekFrom::Start(offset))?;
            self.data.read_exact(&mut buf)?;
            let file_header = FileHeader::try_from(buf.as_slice())?;
            offset += file_header.header_size as u64;
            self.parts.push(file_header);
        }
        Ok(())
    }

    /// Return the raw version of the .pac header
    pub fn raw_header(&mut self) -> Result<RawPacHeader, Error> {
        RawPacHeader::try_from(self.get_bytes(0, PAC_HEADER_LEN)?.as_slice())
    }

    /// Return the raw file header corresponding to index
    pub fn raw_file_header(&mut self, index: usize) -> Result<RawFileHeader, Error> {
        if index < self.parts.len() {
            RawFileHeader::try_from(
                self.get_bytes(self.offset_to_file_header(index), FILE_HEADER_LEN)?
                    .as_slice(),
            )
        } else {
            Err(Error::new(format!(
                "Index out of range (max index: {}",
                self.parts.len()
            )))
        }
    }

    /// Computes the offset to the file header data corresponding to index
    fn offset_to_file_header(&self, index: usize) -> u64 {
        let mut offset = self.header.filetable_offset;
        for i in 0..index {
            offset += self.parts[i].header_size;
        }
        offset as u64
    }

    /// Get a copy of a chunk of bytes from the input file.
    fn get_bytes(&mut self, offset: u64, size: usize) -> Result<Vec<u8>, Error> {
        self.data.seek(SeekFrom::Start(offset))?;
        let mut buf = vec![0; size];
        self.data.read_exact(&mut buf)?;
        Ok(buf)
    }

    /// Extract the files to disk
    pub fn extract_files(&mut self, options: Options) -> Result<(), Error> {
        let mut extract_list = self.parts.clone();
        if !options.index.is_empty() {
            if options
                .index
                .iter()
                .any(|i| !(0..extract_list.len()).contains(i))
            {
                return Err(Error::from("Invalid ID. See ID column of list command"));
            } else {
                extract_list = extract_list
                    .into_iter()
                    .enumerate()
                    .filter(|&(i, _)| options.index.iter().any(|&j| j == i))
                    .map(|(_, e)| e)
                    .collect();
            }
        } else if !options.exclude.is_empty() {
            if options
                .index
                .iter()
                .any(|i| !(0..extract_list.len()).contains(i))
            {
                return Err(Error::from("Invalid ID. See ID column of list command"));
            } else {
                extract_list = extract_list
                    .into_iter()
                    .enumerate()
                    .filter(|&(i, _)| options.exclude.iter().any(|&j| j != i))
                    .map(|(_, e)| e)
                    .collect();
            }
        }

        for part in extract_list {
            if !part.file_flag {
                eprintln!("Warning: not extracting part {}: not a file", part.file_id);
            } else {
                let filename = part.file_name;
                //let offset = (self.header.filetable_offset + part.data_offset) as u64;
                let offset = part.data_offset as u64;
                //let offset = 0x22029B; //offset lo logo.bmp
                let size = part.file_size as usize;
                if options.force {
                    self.write_to_disk_unchecked(&filename, offset, size)?;
                } else {
                    self.write_to_disk(&filename, offset, size)?;
                }
                println!("{filename} extracted.");
            }
        }
        Ok(())
    }

    /// Helper function: writes given data to disk, returns an error if the file
    /// already exists
    fn write_to_disk(&mut self, filename: &str, offset: u64, size: usize) -> Result<(), Error> {
        if File::open(&filename).is_ok() {
            return Err(Error::new(format!("File {} already exists", filename)));
        }
        self.write_to_disk_unchecked(filename, offset, size)
    }

    /// Helper function: writes given data to disk, overwrites an already existing file
    fn write_to_disk_unchecked(
        &mut self,
        filename: &str,
        offset: u64,
        size: usize,
    ) -> Result<(), Error> {
        let mut output_file = File::create(filename)?;
        self.write_to(&mut output_file, offset, size)?;
        Ok(())
    }

    /// Helper function: writes given data to a writer
    fn write_to(&mut self, w: &mut dyn Write, offset: u64, size: usize) -> Result<(), Error> {
        const CAPACITY: usize = 100 * 1024 * 1024; // Set temp buffer capacity to 100MB
        let mut buffer = vec![0; CAPACITY]; // allocate an empty buffer until the specified capacity
        self.data.seek(SeekFrom::Start(offset))?;

        // Buffered copy to the output file
        let mut remaining_bytes = size;
        while remaining_bytes > 0 {
            let buffsize = std::cmp::min(CAPACITY, remaining_bytes);
            buffer.truncate(buffsize);
            self.data.read_exact(&mut buffer)?;
            w.write_all(&buffer)?;
            remaining_bytes -= buffsize;
        }
        Ok(())
    }
}

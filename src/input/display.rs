//! This module defines the implementation of the Display trait for Input
//!
use super::Input;
use crate::file_header::display::TableEntry;
use tabled::Table;

impl std::fmt::Display for Input {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let mut entries = Vec::new();
        for (index, part) in self.parts.iter().enumerate() {
            entries.push(TableEntry::with_index(part, index));
        }
        let header = format!(
            "Filename: {}\nFile size: {} bytes\n",
            self.filename, self.size
        );
        let table = Table::new(entries);
        let display = format!("{}\n{}\n\n{table}", header, self.header);
        write!(f, "{}", display)
    }
}

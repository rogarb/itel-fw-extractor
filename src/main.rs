mod cli;
mod error;
mod file_header;
mod header;
mod input;
mod tabled_types;
mod utils;

use error::Error;

fn main() -> Result<(), Error> {
    cli::Cli::run()
}

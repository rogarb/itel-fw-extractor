//! This module defines some types for easy integration with tabled.

/// Printable hexadecimal type
pub struct HexValue<T>(T);

impl<T: std::fmt::LowerHex> std::fmt::Display for HexValue<T> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "0x{:x}", self.0)
    }
}

/// Implement conversion of an array into HexValue of the corresponding types
macro_rules! impl_try_from_array_and_slice_for_hex_value {
    ($TY: ty, $SIZE: literal) => {
        impl From<[u8; $SIZE]> for HexValue<$TY> {
            fn from(array: [u8; $SIZE]) -> Self {
                Self(<$TY>::from_le_bytes(array))
            }
        }

        impl ::std::convert::TryFrom<&[u8]> for HexValue<$TY> {
            type Error = String;
            fn try_from(values: &[u8]) -> Result<Self, Self::Error> {
                match values.len() {
                    // in this case the length is checked so the unwrap() can't cause a panic
                    $SIZE => Ok(Self(<$TY>::from_le_bytes(values.try_into().unwrap()))),
                    n => Err(format!("Unable to convert: wrong length of array: {n}")),
                }
            }
        }
    };
}

impl_try_from_array_and_slice_for_hex_value!(u8, 1);
impl_try_from_array_and_slice_for_hex_value!(u16, 2);
impl_try_from_array_and_slice_for_hex_value!(u32, 4);
impl_try_from_array_and_slice_for_hex_value!(u64, 8);
impl_try_from_array_and_slice_for_hex_value!(u128, 16);

/// Printable array type, where WIDTH defines the number of bytes displayed per
/// line. This allows printing the data as a hex grid.
pub struct ArrayValue<const WIDTH: usize>(Vec<u8>);

impl<const WIDTH: usize> From<&[u8]> for ArrayValue<WIDTH> {
    fn from(slice: &[u8]) -> Self {
        ArrayValue(Vec::from(slice))
    }
}

impl<const WIDTH: usize, const N: usize> From<[u8; N]> for ArrayValue<WIDTH> {
    fn from(array: [u8; N]) -> Self {
        ArrayValue(Vec::from(array.as_slice()))
    }
}

impl<const WIDTH: usize> std::fmt::Display for ArrayValue<WIDTH> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let mut display = String::new();
        for (i, val) in self.0.iter().enumerate() {
            if i == 0 {
                display.push_str(format!("0x{:02x}", val).as_str());
            } else if i % WIDTH == 0 {
                display.push_str(format!("\n0x{:02x}", val).as_str());
            } else {
                display.push_str(format!(" 0x{:02x}", val).as_str());
            }
        }
        write!(f, "{}", display)
    }
}

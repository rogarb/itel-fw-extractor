//! This module contains various utility functions
//!

use crate::error::Error;

/// Tries to create a String from u16 little endian formatted byte slice
pub fn stringify_u16(data: &[u8]) -> Result<String, Error> {
    if data.len() % 2 != 0 {
        Err(Error::from("can't stringify input, odd len"))
    } else {
        let mut vec = vectorize_u16(data)?;

        // remove trailing null bytes
        while let Some(i) = vec.pop() {
            if i != 0_u16 {
                vec.push(i);
                break;
            }
        }
        // String::from_utf16() returns an error when parsing an empty vector
        if vec.is_empty() {
            Ok(String::new())
        } else {
            Ok(String::from_utf16(&vec)?)
        }
    }
}

/// Tries to create a bool from raw bytes
pub fn boolify(data: &[u8]) -> Result<bool, Error> {
    if data.is_empty() {
        Err(Error::from("boolify: empty input"))
    } else {
        Ok(data.iter().any(|&i| i != 0))
    }
}

/// Tries to create a Vec<u32> out of a slice of raw bytes
pub fn vectorize_u32(data: &[u8]) -> Result<Vec<u32>, Error> {
    vectorize(data, 4).map(|v| v.into_iter().map(|e| e as u32).collect())
}

/// Tries to create a Vec<u32> out of a slice of raw bytes
pub fn vectorize_u16(data: &[u8]) -> Result<Vec<u16>, Error> {
    vectorize(data, 2).map(|v| v.into_iter().map(|e| e as u16).collect())
}

/// Tries to create a vector of n-byte coded values from raw bytes.
///
/// This assumes that the values are in little endian format !
/// The value type is u128 as it will work until n == 16.
fn vectorize(data: &[u8], n: usize) -> Result<Vec<u128>, Error> {
    const MAX: usize = std::mem::size_of::<u128>();
    if n == 0 || n > MAX {
        Err(Error::new(format!("BUG: invalid n value: {n}")))
    } else if data.len() % n != 0 {
        Err(Error::new(format!(
            "BUG: input len is not a multiple of {n}"
        )))
    } else {
        let mut result = Vec::new();
        let mut iter = data.iter();
        while let Some(first) = iter.next() {
            let mut v = vec![*first];
            for _i in 0..n - 1 {
                v.push(*iter.next().expect("BUG: incorrect input data len"));
            }
            for _i in v.len()..MAX {
                v.push(0x00);
            }
            result.push(u128::from_le_bytes(
                v.try_into()
                    .expect("BUG: check implementation of vectorize"),
            ));
        }
        Ok(result)
    }
}

#[cfg(test)]
mod test {
    mod stringify_u16 {
        use crate::utils::stringify_u16;
        #[test]
        fn from_odd_data() {
            let data = vec![0x01, 0x02, 0x03];

            assert!(stringify_u16(&data).is_err());
        }

        #[test]
        fn hello_world() {
            let data = vec![
                0x48, 0x00, // 'H'
                0x65, 0x00, // 'e'
                0x6C, 0x00, // 'l'
                0x6C, 0x00, // 'l'
                0x6F, 0x00, // 'o'
                0x20, 0x00, // ' '
                0x77, 0x00, // 'w'
                0x6F, 0x00, // 'o'
                0x72, 0x00, // 'r'
                0x6C, 0x00, // 'l'
                0x64, 0x00, // 'd'
                0x21, 0x00, // '!'
                0x0A, 0x00, // '\n'
                0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            ];

            assert_eq!(stringify_u16(&data), Ok(String::from("Hello world!\n")));
        }
    }

    mod boolify {
        use crate::utils::boolify;

        #[test]
        fn empty_data() {
            let data = Vec::new();
            assert!(boolify(&data).is_err());
        }

        #[test]
        fn all_null() {
            let data = vec![0x00, 0x00, 0x00];
            assert_eq!(boolify(&data), Ok(false));
        }

        #[test]
        fn some_null() {
            let data = vec![0x00, 0xEF, 0x00];
            assert_eq!(boolify(&data), Ok(true));
        }
    }

    mod vectorize {
        use super::super::vectorize;

        #[test]
        fn vec_u32() {
            use super::super::vectorize_u32;

            let data = vec![
                0x01, 0x00, 0x00, 0x00, // 1
                0x02, 0x00, 0x00, 0x00, // 2
                0x03, 0x00, 0x00, 0x00, // 3
            ];

            assert_eq!(vectorize_u32(&data), Ok(vec![1_u32, 2_u32, 3_u32]));
        }
    }
}
